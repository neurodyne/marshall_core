addSbtPlugin("org.scalameta"   % "sbt-scalafmt" % "2.0.3")
addSbtPlugin("ch.epfl.scala"   % "sbt-scalafix" % "0.9.6")
addSbtPlugin("io.get-coursier" % "sbt-coursier" % "2.0.0-RC3-3")
//addSbtPlugin("io.github.davidgregory084" % "sbt-tpolecat" % "0.1.7")
