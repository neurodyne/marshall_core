val scalaVer      = "2.12.9"
val json4sVersion = "3.6.7"

// Provide a managed dependency on X if -DXVersion="" is supplied on the command line.
val defaultVersions = Map(
  "firrtl"  -> "1.2-SNAPSHOT",
  "chisel3" -> "3.2-SNAPSHOT"
)

libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-jackson" % json4sVersion
)

scalacOptions ++= Seq(
  "-Xsource:2.11",
  //"-Xlint",
  "-Xverify",
  "-feature",
  "-deprecation",
  "-explaintypes",
  "-unchecked",
  "-Xfuture",
  "-encoding",
  "UTF-8",
  "-Yrangepos",
  //"-Xlint:_,-type-parameter-shadow",
  //"-Ywarn-numeric-widen",
  "-Ywarn-unused",
  //"-Ywarn-value-discard",
  "-language:higherKinds",
  //"-language:existentials",
  "-language:reflectiveCalls"
  //"-Yno-adapted-args",
  //"-Ypartial-unification",
  //"-Xfatal-warnings",
  //"-Xlint:-infer-any,_",
  //"-Ywarn-value-discard",
  //"-Ywarn-numeric-widen",
  //"-Ywarn-extra-implicit",
  //"-Ywarn-unused:_",
  //"-Ywarn-inaccessible",
  //"-Ywarn-nullary-override",
  //"-Ywarn-nullary-unit",
  //"-opt-inline-from:<source>",
  //"-opt-warnings",
  //"-opt:l:inline",
)

addCompilerPlugin(scalafixSemanticdb)

lazy val commonSettings = Seq(
  organization := "edu.berkeley.cs",
  version := "1.2",
  scalaVersion := scalaVer,
  crossScalaVersions := Seq(scalaVer, "2.11.12"),
  parallelExecution in Global := false,
  traceLevel := 15,
  maxErrors := 5,
  libraryDependencies ++= Seq("chisel3").map { dep: String =>
    "edu.berkeley.cs" %% dep % sys.props.getOrElse(dep + "Version", defaultVersions(dep))
  },
  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.1" cross CrossVersion.full)
)

lazy val macros = (project in file("macros")).settings(commonSettings)
lazy val marshall = (project in file("."))
  .settings(commonSettings)
  .dependsOn(macros)
//.aggregate(macros) // <-- means the running task on rocketchip is also run by aggregate tasks

addCommandAlias("com", "all compile test:compile it:compile")
addCommandAlias("lint", "; compile:scalafix --check ; test:scalafix --check")
addCommandAlias("fix", "all compile:scalafix test:scalafix")
//addCommandAlias("fmt", "; scalafmtSbt; scalafmtAll; test:scalafmtAll")
addCommandAlias("fmt", "; scalafmtSbt")
addCommandAlias("chk", "; scalafmtSbtCheck; scalafmtCheck; test:scalafmtCheck")
addCommandAlias("cov", "; clean; coverage; test; coverageReport")
